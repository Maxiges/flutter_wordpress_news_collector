import '../main.dart';
import 'package:flutter/material.dart';
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';

import 'dart:async';
import 'dart:convert';
import 'package:http/http.dart' as http;

import 'package:url_launcher/url_launcher.dart';
import 'package:shared_preferences/shared_preferences.dart';


import '../Globals.dart';

import 'WebPortal.dart';




class WebsideInfo {
  String URL, TITTLE, HREF, DATE, DESCRIPTION;
  String LinkColor;

  WebsideInfo(this.URL, this.TITTLE, this.HREF, this.DATE, this.LinkColor,
      this.DESCRIPTION)
  {
    this.TITTLE = this.TITTLE.replaceAll("\u0105", "ą");
    this.TITTLE = this.TITTLE.replaceAll("\u0107", "ć");
    this.TITTLE = this.TITTLE.replaceAll("\u0119", "ę");
    this.TITTLE = this.TITTLE.replaceAll("\u0142", "ł");
    this.TITTLE = this.TITTLE.replaceAll("\u0144", "ń");
    this.TITTLE = this.TITTLE.replaceAll("\u00f3", "ó");
    this.TITTLE = this.TITTLE.replaceAll("\u015b", "ś");
    this.TITTLE = this.TITTLE.replaceAll("\u017a", "ź");
    this.TITTLE = this.TITTLE.replaceAll("\u017c", "ż");

    this.DESCRIPTION = this.DESCRIPTION.replaceAll("\u0105", "ą");
    this.DESCRIPTION = this.DESCRIPTION.replaceAll("\u0107", "ć");
    this.DESCRIPTION = this.DESCRIPTION.replaceAll("\u0119", "ę");
    this.DESCRIPTION = this.DESCRIPTION.replaceAll("\u0142", "ł");
    this.DESCRIPTION = this.DESCRIPTION.replaceAll("\u0144", "ń");
    this.DESCRIPTION = this.DESCRIPTION.replaceAll("\u00f3", "ó");
    this.DESCRIPTION = this.DESCRIPTION.replaceAll("\u015b", "ś");
    this.DESCRIPTION = this.DESCRIPTION.replaceAll("\u017a", "ź");
    this.DESCRIPTION = this.DESCRIPTION.replaceAll("\u017c", "ż");

    this.TITTLE =this.TITTLE.replaceAll("\r" , "");
    this.TITTLE =this.TITTLE.replaceAll("\n" , "");

    this.TITTLE =this.TITTLE.replaceAll("&#8211;" , " ");
    this.DESCRIPTION =this.DESCRIPTION.replaceAll("&#8211;" , " ");
    this.TITTLE =this.TITTLE.replaceAll("&#8217;" , " ");
    this.DESCRIPTION =this.DESCRIPTION.replaceAll("&#8217;" , " ");
    this.TITTLE =this.TITTLE.replaceAll("&#8230;" , " ");
    this.DESCRIPTION =this.DESCRIPTION.replaceAll("&#8230;" , " ");
    this.TITTLE =this.TITTLE.replaceAll("&#8222;" , " ");
    this.DESCRIPTION =this.DESCRIPTION.replaceAll("&#8222;" , " ");
    this.TITTLE =this.TITTLE.replaceAll("&#8221;" , " ");
    this.DESCRIPTION =this.DESCRIPTION.replaceAll("&#8221;" , " ");

    this.TITTLE =this.TITTLE.replaceAll("<p>" , " ");
    this.DESCRIPTION =this.DESCRIPTION.replaceAll("<p>" , " ");
    this.TITTLE =this.TITTLE.replaceAll("</p>" , " ");
    this.DESCRIPTION =this.DESCRIPTION.replaceAll("</p>" , " ");

    this.TITTLE =this.TITTLE.replaceAll("[&hellip;]" , " ");
    this.DESCRIPTION =this.DESCRIPTION.replaceAll("[&hellip;]" , " ");


    this.DESCRIPTION =this.DESCRIPTION.replaceAll("\r" , "");
    this.DESCRIPTION =this.DESCRIPTION.replaceAll("\n" , "");


  }

  Color getColor() {
    try {
      return colorPalet[this.LinkColor];
    } catch (ex) {}
    return Colors.black;
  }

  WebsideInfo_tryRead(String JsonString) {
    try {
      Map<String, dynamic> user = jsonDecode(JsonString);
      this.DATE = user["DATE"];
      this.URL = user["URL"];
      this.TITTLE = user["TITTLE"];
      this.HREF = user["HREF"];
      this.DESCRIPTION = user["DESC"];
      this.LinkColor = user["COLOR"];
    } catch (ex) {
      this.URL = "";
      this.TITTLE = "";
      this.HREF = "";
      this.DATE = "";
      this.DESCRIPTION = "";
    }
  }

  String ToJsonString() {
    String JsonStr = '{ "DATE" : "' +
        this.DATE.replaceAll("\n", "") +
        '", "URL" :"' +
        this.URL.replaceAll("\n", "") +
        '", "TITTLE" :"' +
        this.TITTLE.replaceAll("\n", "") +
        '", "HREF" :"' +
        this.HREF.replaceAll("\n", "") +
        '", "COLOR" :"' +
        this.LinkColor.hashCode.toString().replaceAll("\n", "") +
        '", "DESC" :"' +
        this.DESCRIPTION.replaceAll("\n", "") +
        '"}';

    return JsonStr;
  }
}


Future<List<WebsideInfo>> GetWebsideInfos( WebPortal WEB) async {

  List<WebsideInfo> websideCheeck = new List<WebsideInfo>();
  try {


      try {
        final response =
        await http.get("https://" + WEB.url + "/wp-json/wp/v2/posts?_embed");
        if (response.statusCode == 200) {
          List<dynamic> retJson = json.decode(response.body);
          for (dynamic items in retJson) {

          try {
            dynamic imagersc =
            items['_embedded']['wp:featuredmedia'][0]['media_details']['sizes']['medium']['source_url'];
            String m_article_tittle = items['title']['rendered'];
            String m_article_descrip = items['excerpt']['rendered'];
            websideCheeck.add(new WebsideInfo(
                items['link'],
                m_article_tittle,
                imagersc,
                items['date'],
                GetStringColor(WEB.getColor()),
                m_article_descrip));
          }
          catch(ex){
          }
          }
        }
      }
      catch(ex)
  {
    print("Load Page error :" + ex);
  }
    websideCheeck.sort((a, b) {
      if (DateTime.parse(a.DATE).isBefore(DateTime.parse(b.DATE)) == true)
        return 1;
      else
        return 0;
    });
  }
catch(ex)
  {
 print(ex);
  }
  return websideCheeck;
}

void launchURL(String URL) async {
  if (await canLaunch(URL)) {
    await launch(URL);
  } else {
    throw 'Could not launch $URL';
  }
}

Future<bool> save_WebsideArch(List<WebsideInfo> webObj) async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  List<String> ObjSave = new List<String>();

  for (WebsideInfo objectVal in webObj) {
    ObjSave.add(objectVal.ToJsonString());
  }
  prefs.remove("SaverURLS");
  return prefs.setStringList("SaverURLS", ObjSave);
}

Future<List<WebsideInfo>> load_WebsideArch() async {
  final SharedPreferences prefs = await SharedPreferences.getInstance();
  List<WebsideInfo> redadWebs = new List<WebsideInfo>();
  try {
    List<String> loadedWevs = prefs.getStringList("SaverURLS");
    for (String JsonString in loadedWevs) {
      WebsideInfo newSavedPage =
      new WebsideInfo("", "", "", "", "", "");
      newSavedPage.WebsideInfo_tryRead(JsonString);
      if (newSavedPage.TITTLE.length > 0) {
        redadWebs.add(newSavedPage);
      }
    }
  } catch (ex) {
    print(ex.toString());
  }

  return redadWebs;
}





Animatable<Color> background = TweenSequence<Color>(
  [
    TweenSequenceItem(
      weight: 1.0,
      tween: ColorTween(
        begin: Colors.black,
        end: Colors.white,
      ),
    ),

  ],
);



int savedFileContainsThisWebside(WebsideInfo p_act) {
  int i = 0;
  for (WebsideInfo iter in Global_savedWebside) {
    if (iter.TITTLE == p_act.TITTLE && iter.URL == p_act.URL) {
      return i;
    }
    i++;
  }
  return -1;
}

