import 'package:WP_news_APP/Globals.dart';
import 'package:flutter/material.dart';
import 'package:flutter/material.dart' as prefix0;
import 'package:flutter/painting.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart' as UrlLauncher;
import '../Class/WebsideInfo.dart';

class ShowMoreInfo extends StatefulWidget {
  ShowMoreInfo({Key key}) : super(key: key);

  @override
  _ShowMoreInfo createState() => _ShowMoreInfo();
}

class _ShowMoreInfo extends State<ShowMoreInfo>
    with SingleTickerProviderStateMixin {
  @override
  void initState() {
    super.initState();
  }

  Widget ShowMoreInfo_Fun(WebsideInfo p_webInfo, BuildContext context) {
    return Container(
        width: Global_width,
        margin: new EdgeInsets.all(0),
        child: ListView(children: <Widget>[
          Center(
            child: Container(
              margin: new EdgeInsets.all(5),
              child: new ClipRRect(
                borderRadius: new BorderRadius.circular(8.0),
                child: Hero(
                  tag: p_webInfo.HREF,
                  child: Image.network(
                    p_webInfo.HREF,
                    fit: BoxFit.cover,
                    width: 200,
                    height: 200,
                  ),
                ),
              ),
            ),
          ),
          Container(
            margin: new EdgeInsets.only(bottom: 10 , top: 5 , left: 5 , right: 5),
            child: Text(
              p_webInfo.TITTLE,
              style: TextStyle(fontSize: 20),
            ),
          ),
          Container(
            margin: new EdgeInsets.only(bottom: 10 , top: 5 , left: 5 , right: 5),
            child: Text(
              p_webInfo.DESCRIPTION,
              style: TextStyle(fontSize: 14),
            ),
          ),
          Row(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: <Widget>[
              Expanded(
                child: Container(
                  margin: new EdgeInsets.only(top: 20, left: 5, right: 5),
                  child: FlatButton(
                    color: Colors.greenAccent,
                    child: new Text(
                      "Hide",
                      style: TextStyle(
                        color: Colors.black,
                      ),
                    ),
                    onPressed: () => Navigator.pop(context, true),
                  ),
                ),
              ),
              Expanded(
                child: Container(
                  margin: new EdgeInsets.only(top: 20, left: 5, right: 5),
                  child: FlatButton(
                      color: Colors.blueAccent,
                      child: new Text(
                        "Read more",
                        style: TextStyle(
                          color: Colors.black,
                        ),
                      ),
                      onPressed: () => launchURL(p_webInfo.URL)),
                ),
              ),
            ],
          ),
        ]));
  }

  @override
  Widget build(BuildContext context) {
    final WebsideInfo WebInfo = ModalRoute.of(context).settings.arguments;

    return Scaffold(
        backgroundColor: Colors.black,
        body: ShowMoreInfo_Fun(WebInfo, context));
  }
}
